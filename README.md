# FileTR : File Transmitter Receiver for LAN

### Intent:
	- To Tx/Rx files over a local network
	- Not to use pre-made protocols, FTP, SFTP etc. (only use TCP/UDP) so as to learn the downsides of this and eventually appreciate said protocols
	- To learn Implementations of Networking related Qt functions and C++ STL library functions and understand the differences and relaions
	- To get a better understanding of Networking with TCP
	- To make a program that is a bit more complex than the loopback example in Qt but way simpler than the Torrent Client example
