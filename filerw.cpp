#include "filerw.h"

FileRW::FileRW(QDataStream * dataStream, bool writer , QObject *parent)
	: QObject{parent}, ioDataStream(dataStream), writeEnabled(writer)
{
	filePTR = new QFile (this);
}

void FileRW::setFilePathName(QString fileName)
{
	/// Could have used
	/// std::filesystem::path filePath = fileName.toStdU16String();
	/// To get usable path

	QFileInfo check_file(fileName);
	QDir::setCurrent(check_file.dir().absolutePath());
	filePTR->setFileName(check_file.fileName());
	if(check_file.exists() && check_file.isFile() && writeEnabled)
	{
		emit fileExists();
	}
	else if (((!check_file.exists()) || check_file.isDir()) && (!writeEnabled))
	{
		emit fileDoesNotExist();
	}
}

void FileRW::openFile()
{
	if(writeEnabled)
	{
		if(filePTR->open(QIODevice::ReadWrite))
		{
			emit fileOpenedForWrite();
		}
		else
		{
			emit errorOpeningFile(filePTR->errorString());
		}
	}
	else
	{
		if(filePTR->open(QIODevice::ReadOnly))
		{
			emit fileOpenedForRead();
		}
		else
		{
			emit errorOpeningFile(filePTR->errorString());
		}
	}
}
