#include "controllerwidget.h"
#include "ui_controllerwidget.h"

controllerWidget::controllerWidget(QWidget *parent)
	: QWidget(parent)
	, ui(new Ui::controllerWidget)
{
	ui->setupUi(this);
	transConn = new NetworkConnection (ioDataStream_inFile, true);
	recvConn = new NetworkConnection (ioDataStream_outFile, false);
	inFile = new FileRW (ioDataStream_inFile, false);
	outFile = new FileRW (ioDataStream_outFile, true);
	transConn->moveToThread(&netSenderThread);
	recvConn->moveToThread(&netReceiverThread);
	inFile->moveToThread(&fileInThread);
	outFile->moveToThread(&fileOutThread);
	netReceiverThread.start();
	netSenderThread.start();
	fileInThread.start();
	fileOutThread.start();

	connect(ui->pB_CancelReceive, &QPushButton::clicked, recvConn, &NetworkConnection::severServerConnections);
	connect(ui->pB_AcceptIncomingConnection, &QPushButton::clicked, recvConn, &NetworkConnection::startAcceptingIncomingConnections);
	connect(this, &controllerWidget::incomingConnectionInfo, recvConn, &NetworkConnection::setupServer);
	connect(recvConn, &NetworkConnection::dataReceived, this, &controllerWidget::handleReceiverReceivedData);
	connect(recvConn, &NetworkConnection::errorInCurrentConnection, ui->out_Receiver, &QTextBrowser::append);
	connect(recvConn, &NetworkConnection::incomingConnectionRequestReceived, this, [this] () {	ui->out_Receiver->append(tr("Incoming Connection Request"));	});
	connect(recvConn, &NetworkConnection::incomingConnectionRequestAccepted, this, [this] (QHostAddress address) {	ui->out_Receiver->append(tr("Incoming Connection Accepted with %1").arg(address.toString()));	});
	connect(recvConn, &NetworkConnection::incomingConnectionDisconnected, this, [this] () {	ui->out_Receiver->append("Disconnected from Transmitter");	});
	connect(recvConn, &NetworkConnection::serverStarted, this, [this] () {	ui->out_Receiver->append(tr("Server Started"));	});
	connect(recvConn, &NetworkConnection::serverError, ui->out_Receiver, &QTextBrowser::append);

	connect(ui->pB_CancelTrans, &QPushButton::clicked, transConn, &NetworkConnection::closeSender);
	connect(ui->pB_TransNow, &QPushButton::clicked, this, &controllerWidget::transmitData);
	connect(this, &controllerWidget::outgoingConnectionInfo, transConn, &NetworkConnection::establishOutgoingConnection);
	connect(this, &controllerWidget::dataOut, transConn, &NetworkConnection::sendData);
	connect(transConn, &NetworkConnection::dataSent, this, [this] () {	ui->out_Transmitter->append("Data Sent");	});
	connect(transConn, &NetworkConnection::outgoingConnectionRequestSent, this, [this] () {	ui->out_Transmitter->append("Trying to Connect ...");	emit ui->pB_AcceptIncomingConnection->clicked();});
	connect(transConn, &NetworkConnection::outgoingConnectionEstablished, this, [this] () {	ui->out_Transmitter->append("Outgoing Connection Established");	});
	connect(transConn, &NetworkConnection::outgoingConnectionDisconnected, this, [this] () {	ui->out_Transmitter->append("Disconnected");	});
	connect(transConn, &NetworkConnection::errorSendingData, ui->out_Transmitter, &QTextBrowser::append);
}

controllerWidget::~controllerWidget()
{
	netSenderThread.quit();
	netSenderThread.wait();
	netReceiverThread.quit();
	netReceiverThread.wait();
	fileInThread.quit();
	fileInThread.wait();
	fileOutThread.quit();
	fileOutThread.wait();
	delete ui;
}

void controllerWidget::on_tB_SetReceiverAttributes_clicked()
{
	emit incomingConnectionInfo(QHostAddress(ui->lE_ReceiveOwnIP->text()), ui->sB_ReceiveOwnPort->value(), QHostAddress(ui->lE_ReceiveFromIP->text()), ui->spB_ReceiveFromPort->value());
}

void controllerWidget::handleReceiverReceivedData(QByteArray inData)
{
	ui->out_Receiver->append(inData);
}

void controllerWidget::transmitData()
{
	emit dataOut(ui->tE_DataInput->toHtml().toUtf8());
}


void controllerWidget::on_tB_ConnectToReceiver_clicked()
{
	emit outgoingConnectionInfo(ui->lE_transDestIP->text(), ui->spB_TransOwnPort->value(), ui->spB_TransDestPort->value());
}

