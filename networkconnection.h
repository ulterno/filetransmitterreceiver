#ifndef NETWORKCONNECTION_H
#define NETWORKCONNECTION_H

#include <QObject>
#include <QTcpSocket>
#include <QTcpServer>
#ifdef QT_DEBUG
#include <QDebug>
#endif // QT_DEBUG

#if QT_VERSION < QT_VERSION_CHECK(6,0,0)
#include <QMetaType>
Q_DECLARE_METATYPE(QHostAddress)
#endif // QT_VERSION check
class NetworkConnection : public QObject
{
	Q_OBJECT
public:
	explicit NetworkConnection(QDataStream *dataStream, bool outgoing, QObject *parent = nullptr);
	QDataStream *ioDataStream;

signals:

	void outgoingConnectionRequestSent();
	void outgoingConnectionEstablished();
	void dataSent();
	void outgoingConnectionDisconnected();
	void errorSendingData(QString errorString);

	void serverStarted();
	void serverError(QString errorString);
	void incomingConnectionRequestReceived();
	void incomingConnectionRequestAccepted(QHostAddress peerAddress);
	void dataReceived (QByteArray inData);
	void incomingConnectionDisconnected();
	void errorInCurrentConnection(QString errorString);

public slots:

	void establishOutgoingConnection(QString hostAddress, quint16 selfportNo, quint16 targetPortNo);
	void sendData(QByteArray outData);
	void closeSender();
	void startAcceptingIncomingConnections();
	void setupServer(QHostAddress bindAddress, quint16 portNo, QHostAddress acceptAddress, quint16 acceptPort);

	void severServerConnections();

private slots:
	void acceptIncomingConnection();

	void indicateConnectionToServer();

	void receiveData();

private:
	bool transmitter;
	QTcpSocket *sender{};
	QTcpServer *server{};
	QTcpSocket *newServerConnection{};

	QHostAddress allowedAddress;
	quint16 allowedPort;
};

#endif // NETWORKCONNECTION_H
