#include "networkconnection.h"

NetworkConnection::NetworkConnection(QDataStream * dataStream, bool outgoing, QObject *parent)
	: QObject{parent}, ioDataStream(dataStream), transmitter(outgoing)
{
#if QT_VERSION < QT_VERSION_CHECK(6,0,0)
	qRegisterMetaType<QHostAddress>("QHostAddress");
#endif
	if (transmitter)
	{
		sender = new QTcpSocket (this);
#ifdef QT_DEBUG
		qDebug () << "Transmitter (new): " << sender;
#endif //QT_DEBUG
	}
	else
	{
		server = new QTcpServer (this);
#ifdef QT_DEBUG
		qDebug () << "Receiver (new): " << server;
#endif //QT_DEBUG
	}
}

void NetworkConnection::establishOutgoingConnection(QString hostAddress, quint16 selfPortNo, quint16 targetPortNo)
{
	bool bindSuccessVal = sender->bind(QHostAddress::AnyIPv4, selfPortNo, QTcpSocket::ShareAddress);
#ifdef QT_DEBUG
		qDebug () << "Transmitter Bind at " << sender->localAddress().toString() << ":" << QString::number(sender->localPort()) << " Success? " << bindSuccessVal;
#endif //QT_DEBUG
	connect(sender, &QTcpSocket::connected, this, &NetworkConnection::indicateConnectionToServer, Qt::UniqueConnection);
	connect(sender, &QTcpSocket::disconnected, this, [this] () {	emit outgoingConnectionDisconnected();	});
	sender->connectToHost(QHostAddress(hostAddress), targetPortNo);
	//sender->connectToHost(server->serverAddress(), server->serverPort());
#ifdef QT_DEBUG
		qDebug () << "Sender trying to connect to host at " << QHostAddress(hostAddress) << ":" << QString::number(targetPortNo);
#endif //QT_DEBUG
	emit outgoingConnectionRequestSent();
}

void NetworkConnection::sendData(QByteArray outData)
{
#ifdef QT_DEBUG
		qDebug () << "Transmitter: Checking if data can be sent ...";
#endif //QT_DEBUG
	if(sender->isWritable())
	{
#ifdef QT_DEBUG
		qDebug () << "Transmitter: Sending Data";
#endif //QT_DEBUG
		if(sender->write(outData) > 0)
		{
			emit dataSent();
#ifdef QT_DEBUG
		qDebug () << "Transmitter: Data sending successful";
#endif //QT_DEBUG
		}
		else
		{
#ifdef QT_DEBUG
		qDebug () << "Transmitter: Data Sending Failed : " << sender->errorString();
#endif //QT_DEBUG
			emit errorSendingData(sender->errorString());
		}
	}
	else
	{
		QString socketState;
		switch(sender->state())
		{
		case QAbstractSocket::UnconnectedState:
			socketState = "Unconnected State";
			break;
		case QAbstractSocket::HostLookupState:
			socketState = "Host Lookup State";
			break;
		case QAbstractSocket::ConnectingState:
			socketState = "Connecting State";
			break;
		case QAbstractSocket::BoundState:
			socketState = "Bound State";
			break;
		case QAbstractSocket::ClosingState:
			socketState = "Closing State";
			break;
		case QAbstractSocket::ListeningState:
			socketState = "Listening State";
			break;
		case QAbstractSocket::ConnectedState:
			socketState = "Connected State";
			break;
		default:
			socketState = "Unknown State";
		}
#ifdef QT_DEBUG
		qDebug () << "Transmitter: Unable to write to Socket. Socket state: " << socketState;
#endif //QT_DEBUG

		emit errorSendingData("Socket not writable. State: " + socketState);
	}
}

void NetworkConnection::setupServer(QHostAddress bindAddress, quint16 portNo, QHostAddress acceptAddress, quint16 acceptPort)
{
	allowedPort = acceptPort;
	allowedAddress = acceptAddress;
#ifdef QT_DEBUG
		qDebug () << "Receiver: Accepted Address: " << allowedAddress.toString() << "Accepted Port: " << QString::number(allowedPort) ;
#endif //QT_DEBUG
	if(server->listen(bindAddress, portNo))
	{
#ifdef QT_DEBUG
		qDebug () << "Receiver: Bound to: " << server->serverAddress().toString() << ":" << QString::number(server->serverPort());
#endif //QT_DEBUG
		connect(server, &QTcpServer::newConnection, this, &NetworkConnection::acceptIncomingConnection, Qt::UniqueConnection);
		emit serverStarted();
	}
	else
	{
#ifdef QT_DEBUG
		qDebug () << "Receiver: Unable to start listening: " << server->errorString();
#endif //QT_DEBUG
		emit serverError(server->errorString());
	}
}

void NetworkConnection::closeSender()
{
	if(sender->isOpen())
	{
#ifdef QT_DEBUG
		qDebug () << "Closing Sender ...";
#endif //QT_DEBUG
		sender->disconnectFromHost();
		sender->close();
	}
}

void NetworkConnection::startAcceptingIncomingConnections()
{
	connect(server, &QTcpServer::newConnection, this, &NetworkConnection::acceptIncomingConnection, Qt::UniqueConnection);
}

void NetworkConnection::severServerConnections()
{
#ifdef QT_DEBUG
	qDebug () << "NewServerConnection = " << newServerConnection;
#endif //QT_DEBUG
	if(newServerConnection != 0x0)
	{
		if(newServerConnection->isOpen())
		{
			disconnect(newServerConnection, &QTcpSocket::readyRead, this, &NetworkConnection::receiveData);
			disconnect(newServerConnection, &QTcpSocket::errorOccurred, nullptr, nullptr);
			disconnect(newServerConnection, &QTcpSocket::disconnected, nullptr, nullptr);
			newServerConnection->abort();
	#ifdef QT_DEBUG
			qDebug () << "Closing Receiver";
	#endif //QT_DEBUG
		}
	}
	if(server->isListening())
	{
#ifdef QT_DEBUG
		qDebug () << "Closing Server ...";
#endif //QT_DEBUG
		server->pauseAccepting();
		server->close();
	}
}

void NetworkConnection::acceptIncomingConnection()
{
	newServerConnection = server->nextPendingConnection();
	if(!newServerConnection)
	{
		//[] Nothing to accept
#ifdef QT_DEBUG
		qDebug () << "Server Connection: Nothing New to Connect to.";
#endif //QT_DEBUG
	}
	else
	{
		if(newServerConnection->peerAddress() == allowedAddress)
		{
			if(newServerConnection->peerPort() == allowedPort)
			{
	#ifdef QT_DEBUG
			qDebug () << "Match found. Starting Receiver ...";
	#endif //QT_DEBUG
				emit incomingConnectionRequestAccepted(newServerConnection->peerAddress());
				connect(newServerConnection, &QTcpSocket::readyRead, this, &NetworkConnection::receiveData, Qt::UniqueConnection);
				connect(newServerConnection, &QTcpSocket::errorOccurred, this, [this] () {	emit errorInCurrentConnection(newServerConnection->errorString());	});
				connect(newServerConnection, &QTcpSocket::disconnected, this, [this] () {
	#ifdef QT_DEBUG
					qDebug () << "NewServerConnection Disconnected.";
	#endif //QT_DEBUG
					emit errorInCurrentConnection("New Server Connection Disconnected");
					newServerConnection->close();
				});
				server->close();
				disconnect(server, &QTcpServer::newConnection, this, &NetworkConnection::acceptIncomingConnection);
			}
			else
			{
	#ifdef QT_DEBUG
				qDebug() << "Receiver: Received Connection from Wrong Port";
	#endif //QT_DEBUG
				emit errorInCurrentConnection(tr("Nothing to Connect to"));
			}
		}
	}
}

void NetworkConnection::indicateConnectionToServer()
{
#ifdef QT_DEBUG
		qDebug () << "Sender: Connection to Server Established";
#endif //QT_DEBUG
	disconnect(sender, &QTcpSocket::connected, this, &NetworkConnection::indicateConnectionToServer);
	emit outgoingConnectionEstablished();
}

void NetworkConnection::receiveData()
{
#ifdef QT_DEBUG
	QByteArray inData = newServerConnection->readAll();
	qDebug () << "Receiver: Data Received" << inData;
	emit dataReceived(inData);
#else
	emit dataReceived(newServerConnection->readAll());
#endif //QT_DEBUG
}
