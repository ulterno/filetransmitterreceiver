#ifndef CONTROLLERWIDGET_H
#define CONTROLLERWIDGET_H

#include "networkconnection.h"
#include "filerw.h"
#include <QWidget>
#include <QThread>

QT_BEGIN_NAMESPACE
namespace Ui { class controllerWidget; }
QT_END_NAMESPACE

class controllerWidget : public QWidget
{
	QThread fileInThread, fileOutThread, netReceiverThread, netSenderThread;
	Q_OBJECT

public:
	controllerWidget(QWidget *parent = nullptr);
	~controllerWidget();

signals:
	void startReceiving();
	void startSending();
	void sendCheckPacket();
	void dataOut(QByteArray dataBeingOuted);
	void outgoingConnectionInfo (QString targetHostAddress, quint16 selfPortNo, quint16 targetPortNo);
	void incomingConnectionInfo (QHostAddress bindAddress, quint16 portNo, QHostAddress acceptAddress, quint16 acceptPort);

private slots:

	void on_tB_SetReceiverAttributes_clicked();

	void handleReceiverReceivedData(QByteArray inData);

	void transmitData();

	void on_tB_ConnectToReceiver_clicked();

private:
	Ui::controllerWidget *ui;
	QDataStream *ioDataStream_inFile, *ioDataStream_outFile;
	FileRW *inFile, *outFile;
	NetworkConnection *transConn, *recvConn;
};
#endif // CONTROLLERWIDGET_H
