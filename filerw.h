#ifndef FILERW_H
#define FILERW_H

#include <QObject>
#include <QFile>
#include <QFileInfo>
#include <QDir>

class FileRW : public QObject
{
	Q_OBJECT
public:
	explicit FileRW(QDataStream *dataStream, bool writer, QObject *parent = nullptr);
	QDataStream *ioDataStream;

signals:
	void fileExists();
	void fileDoesNotExist();

	void fileOpenedForWrite();
	void fileOpenedForRead();
	void errorOpeningFile(QString error);

public slots:
	void setFilePathName (QString fileName);
	void openFile ();

private:
	bool writeEnabled;
	QFile *filePTR;
};

#endif // FILERW_H
